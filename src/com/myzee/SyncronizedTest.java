/*
 * calling one thread with synchronized method and another thread with 
 * non-synchronized method on same resource object
 * Resulting in irregular output.
 * 
 * if two thread calls two different synchronized methods on same resource object, 
 * then expect regular output.
 */
package com.myzee;

public class SyncronizedTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Resource r1 = new Resource();
		
		Thread t1 = new Thread(new ResourceExe1(r1));
		Thread t2 = new Thread(new ResourceExe2(r1));
		
		t1.start();
		t2.start();
		
	}

}

class Resource {
	
	/*
	 * Synchronized method
	 */
	public synchronized void access1() throws InterruptedException{
		for (int i = 1; i <= 5; i++) {
//			Thread.sleep(500);
			System.out.println("access1: i is " + i);
		}
	}
	
	/*
	 * Non synchronized method
	 */
	public void access2() throws InterruptedException{
		for (int j = 1; j <= 5; j++) {
//			Thread.sleep(500);
			System.out.println("access2: j is " + j);
		}
	}
}

class ResourceExe1 implements Runnable {
	
	Resource r;
	
	public ResourceExe1( Resource r) {
		this.r = r;
	}
	
	@Override
	public void run(){
		try {
			//calling synchronized method of object r1
			r.access1();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}

class ResourceExe2 implements Runnable {
	
	Resource r;
	
	public ResourceExe2( Resource r) {
		this.r = r;
	}
	
	@Override
	public void run() {
		try {
			//calling non synchronized method of object r1
			r.access2();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}

